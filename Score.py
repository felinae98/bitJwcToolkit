import basic
from bs4 import BeautifulSoup as bs
import bs4
class ScoreSet(dict):
    def __init__(self, response):
        soup = bs(response, 'html5lib')
        tbody = soup.find(id = 'DataGrid1').find('tbody')
        for i in tbody.children:
            if isinstance(i, bs4.element.Tag) and i.get('class') and 'datagridhead' in i.get('class'): continue
            if isinstance(i, bs4.element.NavigableString): continue
            lis = i.find_all('td')
            self[lis[1].string] = Score(float(lis[3].string), float(lis[6].string), lis[2].string)

class Score(dict):
    def __init__(self, score, credit, classType):
        self['score'] = score
        self['credit'] = credit
        self['classType'] = classType

class GetScore(basic.JwcMain):

    class OutOfRange(Exception):
        pass

    def __init__(self):
        super(GetScore, self).__init__()
        self.functionPageUrl = self.functionEntrace['成绩']
        response = self.get(self.functionPageUrl)
        soup = bs(response.text, 'html5lib')
        self.viewstate = soup.find(attrs={'name':'__VIEWSTATE'}).attrs['value']
        self.getValidYear(soup)
        self.getValidSemester(soup)

    def getValidYear(self, soup):
        XNlist = soup.find(id = 'ddlXN')
        self.yearlist = []
        for i in XNlist.children:
            if not isinstance(i, bs4.element.Tag) or i.get('selected') == 'selected': continue
            self.yearlist.append(i.text)
        self.yearlist.remove('')

    def getValidSemester(self,soup):
        XQList = soup.find(id = 'ddlXQ')
        self.semesterList = []
        for i in XQList.children:
            if not isinstance(i, bs4.element.Tag) or i.get('selected') == 'selected': continue
            self.semesterList.append(i.text)
        self.semesterList.remove('')

    def queryAll(self):
        params = {
            'Button2': '%D4%DA%D0%A3%D1%A7%CF%B0%B3%C9%BC%A8%B2%E9%D1%AF',
            '__VIEWSTATE': self.viewstate,
            'ddlXN': '',
            'ddlXQ': ''
        }
        response = self.post(url = self.functionPageUrl, data = params)
        scoreset = ScoreSet(response.text)
        return scoreset

    def queryForAnnual(self, year):
        year = year.strip()
        if not year in self.yearlist:
            raise self.OutOfRange
        params = {
           'Button5': '%B0%B4%D1%A7%C4%EA%B2%E9%D1%AF',
            '__VIEWSTATE': self.viewstate,
            'ddlXN': year,
            'ddlXQ':''
        }
        response = self.post(url = self.functionPageUrl, data = params)
        scoreset = ScoreSet(response.text)
        return scoreset

    def queryForSemester(self, year, semester):
        year = year.strip()
        semester = semester.strip()
        if not year in self.yearlist or not semester in self.semesterList:
            raise self.OutOfRange
        params = {
            'Button1': '%B0%B4%D1%A7%C6%DA%B2%E9%D1%AF',
            '__VIEWSTATE': self.viewstate,
            'ddlXN': year,
            'ddlXQ': semester
        }
        response = self.post(url = self.functionPageUrl ,data = params)
        scoreset = ScoreSet(response.text)
        return scoreset

a = GetScore()
print(a.queryForAnnual('2016-2017'))
print(a.queryForSemester('2014-2015', '3'))
